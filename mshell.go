package main

import (
	"fmt"
	"math"
	"bufio"
	"os"
	"strings"
	"regexp"
	"strconv"
)

func getFizzBuzz(limit int) {
  fmt.Println("Printing FizzBuzz with limit of " + strconv.Itoa(limit))
  var output string = ""
  for i:=0; i < limit; i++ {
    output = ""
    if i % 3 == 0 {
      output = output + "Fizz"
      }
    if i % 5 == 0 {
      output = output + "Buzz"
    }
    if i % 3 != 0 && i % 5 != 0 {
      output = strconv.Itoa(i)
    }
    fmt.Println(output)
  }
}


func getPrimes(limit int) {
  fmt.Println("Calculating primes with limit of " + strconv.Itoa(limit))
  var hasFactor bool
  for i:=2; i < limit; i++ {
    hasFactor = false
    for p:=2; p < int(math.Sqrt(float64(i))) + 1 ; p++ {
      if i % p == 0 {
        hasFactor = true
        continue
      }
    }
    if hasFactor == false {
      fmt.Println(i)
    }
  }
}

func getlimit() {

var limit int = 0
var err error
re := regexp.MustCompile(`^\d+$`)

  reader := bufio.NewReader(os.Stdin)
  fmt.Println("Mleczny Shell v0.1")
  fmt.Println("---------------------")
  fmt.Println("Type help to start...")

  for {
    fmt.Print("-> ")
    text, _ := reader.ReadString('\n')
    // cut EOL for string comparison
    text = strings.Replace(text, "\n", "", -1)
    text = strings.Replace(text, "\r", "", -1)

    if strings.Compare("hi", text) == 0 {
      fmt.Println("hello, Yourself")
    }

    if len(re.FindString(text)) > 0 {
      fmt.Println("number detected")
      fmt.Println("limit = " + text)
      limit, err = strconv.Atoi(text)
      if err != nil {
        fmt.Println(err)
        continue
      }
      //fmt.Println(math.Sqrt(float64(limit)))
    }

    if strings.Compare("fizzbuzz", text) == 0 || strings.Compare("FizzBuzz", text) == 0 {
      getFizzBuzz(limit)
    }

    if strings.Compare("prime", text) == 0 {
      getPrimes(limit)
    }

    if strings.Compare("exit", text) == 0 {
      fmt.Println("goodbye")
      break
    }

    if strings.Compare("help", text) == 0 {
      fmt.Println("---------------------")
      fmt.Println("Try following commands: ")
      fmt.Println("-> hi ")
      fmt.Println("-> 20 ")
      fmt.Println("-> fizzbuzz ")
      fmt.Println("-> prime ")
      fmt.Println("-> exit ")
      fmt.Println("---------------------")
    }


  }

}

func main() {

	getlimit()

}
